# TP3 - Gestion de contextes : ordonnancement, ordonnancement sur interruptions

## Compilation

Pour compiler le TP :

```
make install
```

## Éxecution

Pour éxecuter le programme :

```bash
./build/bin/prod_cons
```