typedef void (funct_t) (void *);

enum ctx_state_e {
  CTX_INIT,
  CTX_EXEC,
  CTX_END,
  CTX_BLK
};

struct ctx_s {
    unsigned int ctx_magic;
    void * ctx_esp;
    void * ctx_ebp;
    unsigned char * ctx_base;
    funct_t * ctx_f;
    void * ctx_arg;
    enum ctx_state_e ctx_state;
    struct ctx_s * ctx_next;
    struct ctx_s * ctx_sem_next;
};

void init_ctx(struct ctx_s *ctx, int stack_size, funct_t f, void *arg);
void create_ctx(int stack_size, funct_t f, void *args);
void yield();

void f_ping(void *arg);
void f_pong(void *arg);
void irq_enable();
void irq_disable();

void switch_to_ctx(struct ctx_s *ctx);

void start_current_ctx();

static void * main_ebp;
static void * main_esp;
struct ctx_s* current_ctx;

