#include "switch_to.h"

typedef
struct sem_s {
    int sem_cpt;
    struct ctx_s * sem_ctx_list;
}sem_s;

void sem_init(sem_s*, unsigned int);
void sem_down(sem_s*);
void sem_up(sem_s*);
