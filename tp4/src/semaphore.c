#include "../include/semaphore.h"
#include "../include/timer.h"
#include <stdlib.h>
#include <stdio.h>

#define MAX_ITERATIONS 500

static int counter = 0;

void sem_init(struct sem_s *sem, unsigned int val) {
    sem->sem_cpt = val;
    sem->sem_ctx_list = NULL;
}

void sem_up(struct sem_s *sem) {
    if (counter <= MAX_ITERATIONS) {
        counter++;
        irq_disable();
        sem->sem_cpt++;
        if (sem->sem_cpt <= 0) {
            sem->sem_ctx_list->ctx_state = CTX_EXEC;
            sem->sem_ctx_list = sem->sem_ctx_list->ctx_sem_next;
        }
        irq_enable();
    } else {
        exit(0);
    }
}

void sem_down(struct sem_s *sem) {

    irq_disable();
    sem->sem_cpt--;

    if(sem->sem_cpt < 0) {
        current_ctx->ctx_state = CTX_BLK;
        current_ctx->ctx_sem_next = sem->sem_ctx_list;
        sem->sem_ctx_list = current_ctx;
        irq_enable();
        yield();
    } else {
        irq_enable();
    }
}