#include <stdio.h>
#include <assert.h>
#include "try.h"

#define CTX_MAGIC 0xCAFEBABE

int try(struct ctx_s *pctx, funct_t *f, int arg) {
    assert(pctx);
    pctx->ctx_magic = CTX_MAGIC;
    asm("movl %%esp, %0" "\n\t" "movl %%ebp, %1" : "=r" (pctx->ctx_esp), "=r" (pctx->ctx_ebp));
    return f(arg);
}

int throw(struct ctx_s *pctx, int r) {
  assert(pctx->ctx_magic == CTX_MAGIC);
  static int copy_r;
  copy_r = r;

  asm("movl %0, %%esp" "\n\t" "movl %1, %%ebp" : : "r" (pctx->ctx_esp), "r" (pctx->ctx_ebp) );

    return copy_r;
}
