# TP1

## Compilation

Pour tout compiler :

```
make clean install
```

Pour compiler `try_mul` :

```
make try_mul
```

Pour compiler `display_stack` :

```
make display_stack
```

## Tests

Pour lancer une procédure de test :

```
./tests.sh
```

## Explications sur `display_stack`

1. On voit que lorsque l'on fait des appels successifs les adresses des registres
ebp et esp sont initialisé une fois au premier appel et ne change plus par la suite.
Pour les appels imbriqués le frame prend des nouvelles adresses pour ebp, esp à
chaque niveau d'imbrication.

2. On voit que les variables locales sont comprises entre les adresses ebp, esp du
frame courant.
