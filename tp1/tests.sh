YELLOW='\033[1;33m'
GREEN='\033[0;32m'
TITLE_COLOR='\033[1;36m'
NC='\033[0m' # No Color

wait_for_keyboard() {
	echo -e "${GREEN}Press any key to run next test... ${NC}"
	read -p "" -n1 -s
	echo -e ""
}

echo -e "${YELLOW}Testing try_mul with [1, 2, 3, 4, 5] ${NC}"
echo
./try_mul << EOF
    1 2 3 4 5
EOF
echo
wait_for_keyboard


echo -e "${YELLOW}Testing display_stack ${NC}"
echo
./display_stack
echo
wait_for_keyboard

