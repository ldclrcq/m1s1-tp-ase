typedef int (funct_t) (int);

struct ctx_s {
    unsigned int ctx_magic;
    void * ctx_esp;
    void * ctx_ebp;
};

int try(struct ctx_s *pctx, funct_t *f, int arg);

int throw(struct ctx_s *pctx, int r);
