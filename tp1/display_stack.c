#include <stdio.h>
#include <assert.h>

void show_registers();
void a();
int rec(int n);

static int * esp;
static int * ebp;

int main() {
  asm("movl %%esp, %0" "\n\t" "movl %%ebp, %1" : "=r" (esp), "=r" (ebp) : );

  printf("Contexte de main\n");
  printf("esp: %p\n", esp);
  printf("ebp: %p\n", ebp);

  printf("Appels imbriqués\n");
  rec(3);

  printf("Appels successifs\n");
  show_registers();
  show_registers();
  show_registers();

  return 0;
}

void show_registers() {
  asm("movl %%esp, %0" "\n\t" "movl %%ebp, %1" : "=r" (esp), "=r" (ebp) : );

  printf("esp: %p\n", esp);
  printf("ebp: %p\n", ebp);
}

int rec(int n) {
  int useless_int = 0;
  int to_prove_my_point = 42;

  asm("movl %%esp, %0" "\n\t" "movl %%ebp, %1" : "=r" (esp), "=r" (ebp) : );

  // On vérifie que les variables locales sont bien dans la frame courante
  assert(&useless_int <= ebp && &useless_int >= esp);
  assert(&to_prove_my_point <= ebp && &to_prove_my_point >= esp);

  // On vérifie que n est bien dans une frame précédente
  assert(&n > ebp);

  printf("Contexte appel récursif : %d\n", n);
  printf("esp: %p\n", esp);
  printf("ebp: %p\n", ebp);

  if (n < 2)
    return n;
  else
    return rec(n-1);
}
