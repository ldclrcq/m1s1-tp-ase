#include <stdio.h>
#include <stdlib.h>
#include "hardware.h"
#include "hw_config.h"
#include "swap.h"
#include "mmu_manager.h"

static struct vm_mapping_s vm_mapping[VM_PAGES];
static struct pm_mapping_s pm_mapping[PM_PAGES];

static int rr_ppage = 1;

int main() {
	unsigned int i;

	if (init_hardware(HARDWARE_INI) == 0) {
		exit(EXIT_FAILURE);
	}

	IRQVECTOR[MMU_IRQ] = mmu_handler;
	_mask(0x1001);

	for (i = 0; i < VM_PAGES; i++) {
		vm_mapping[i].vm_mapped = 0;
	}

	for (i = 0; i < PM_PAGES; i++) {
		pm_mapping[i].pm_mapped = 0;
	}

	user_process();
	return 0;
}

static void mmu_handler(void) {
	int vaddr;
	unsigned int vpage;
	struct tlb_entry_s tlbe;

	vaddr = _in(MMU_FAULT_ADDR);

	if ((vaddr & 0xFFFFFF) == ((int) virtual_memory)) {
		fprintf(stderr, "Virtual address %d is invalid\n", vaddr);
		return;
	}

	vpage = vaddr >> 12 & 0xFFF;

	if (vm_mapping[vpage].vm_mapped) {
		tlbe.tlbe_vpage = vpage;
		tlbe.tlbe_ppage = vm_mapping[vpage].vm_ppage;
		tlbe.tlbe_xwr = 7;
		tlbe.tlbe_access = 1;

		_out(TLB_ADD_ENTRY, *((int *) (&tlbe)));
		return;
	}

	if (pm_mapping[rr_ppage].pm_mapped) {
		store_to_swap(pm_mapping[rr_ppage].pm_vpage, rr_ppage);
		vm_mapping[pm_mapping[rr_ppage].pm_vpage].vm_mapped = 0;
	}

	tlbe.tlbe_vpage = pm_mapping[rr_ppage].pm_vpage;
	tlbe.tlbe_ppage = rr_ppage;
	tlbe.tlbe_xwr = 7;
	tlbe.tlbe_access = 1;

	_out(TLB_DEL_ENTRY, *((int *) (&tlbe)));

	fetch_from_swap(vpage, rr_ppage);

	vm_mapping[vpage].vm_mapped = 1;
	vm_mapping[vpage].vm_ppage = rr_ppage;

	pm_mapping[rr_ppage].pm_mapped = 1;
	pm_mapping[rr_ppage].pm_vpage = vpage;

	tlbe.tlbe_vpage = vpage;
	tlbe.tlbe_ppage = rr_ppage;
	tlbe.tlbe_xwr = 7;
	tlbe.tlbe_access = 1;

	_out(TLB_ADD_ENTRY, *((int *) (&tlbe)));

	rr_ppage++;
	if (rr_ppage == PM_PAGES) {
		rr_ppage = 1;
	}
}