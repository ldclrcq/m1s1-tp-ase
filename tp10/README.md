# TP10 - Mémoire virtuelle et swap disque 

## Compilation

```bash
make
```

## Exécution

```bash
./mmu_manager | ./oracle
```

## Nettoyage

```bash
make clean
```