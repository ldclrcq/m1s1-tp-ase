struct tlb_entry_s {
	unsigned tlbe_rfu :8;
	unsigned tlbe_vpage :12;
	unsigned tlbe_ppage :8;
	unsigned tlbe_xwr :3;
	unsigned tlbe_access :1;
};

struct vm_mapping_s {
	unsigned vm_ppage: 8;
	unsigned vm_mapped: 1;
};

struct pm_mapping_s {
	unsigned pm_vpage: 12;
	unsigned pm_mapped: 1;
};

extern void user_process();

static void mmu_handler(void);