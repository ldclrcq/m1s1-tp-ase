#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "super_b.h"
#include "mbr.h"
#include "vol.h"

struct super_bloc_s super_bloc; 
unsigned int current_vol = 0;

void init_super(unsigned int vol, int serial, char* name) {
    int i; 
    struct free_bloc_s free_bloc; 

    super_bloc.magic = MAGIC_SB;
    super_bloc.serial = serial;
    super_bloc.num_bloc = BLOC_NULL;
    super_bloc.nb_free_bloc = gMBR.vol[vol].nb_bloc - 1; 
    super_bloc.first_free_bloc = 1; 
    strcpy(super_bloc.name, name);

    for(int i=1; i < gMBR.vol[vol].nb_bloc -1; i++) {
        free_bloc.magic = MAGIC_FL; 
        free_bloc.next = i + 1; 
        write_bloc_n(vol, i, sizeof(free_bloc), (unsigned char*) &free_bloc);
    }

    free_bloc.next = 0; 
    free_bloc.next = 1;
    write_bloc_n(vol, i, sizeof(free_bloc), (unsigned char*) &free_bloc);
    current_vol = vol; 
    save_super();
}

int load_super(unsigned int vol) {
    if(vol >= gMBR.nbVol) {
        /*todo*/
        return 0;
    } 

    read_bloc_n(vol, 0, sizeof(super_bloc), (unsigned char*) &super_bloc); 

    if(super_bloc.magic != MAGIC_SB) {
        /* todo */ 
        return 0; 
    }
    
    current_vol = vol;
    
    return 1;
}

void save_super() {
    write_bloc_n(current_vol, 0, sizeof(super_bloc), (unsigned char*) &super_bloc); 
}

unsigned int new_bloc() {
    unsigned int res; 
    struct free_bloc_s free_list; 
    assert(super_bloc.magic == MAGIC_SB);

    res = super_bloc.first_free_bloc;
    if(res == BLOC_NULL || super_bloc.nb_free_bloc == 0 ) {
        printf("No free bloc on volume");
        return BLOC_NULL;
    }

    read_bloc_n(current_vol, res, sizeof(free_list), (unsigned char*) &free_list);
    super_bloc.first_free_bloc = free_list.next;
    save_super();
    
    return res;
}

int free_bloc(unsigned int bloc) {
    struct free_bloc_s bloc_list;
    unsigned int tmp_bloc;

    assert(super_bloc.magic == MAGIC_SB);

    if(load_super(current_vol == 0)) {
        /* todo */ 
        return 0; 
    }

    if (bloc >= (unsigned int) gMBR.vol[current_vol].nb_bloc -1) {
        /* todo */ 
        return 0;
    }

    if (super_bloc.nb_free_bloc == gMBR.vol[current_vol].nb_bloc -1) {
        /* todo */
        return 0;
    	read_bloc_n(current_vol, super_bloc.first_free_bloc, sizeof(bloc_list), (unsigned char*) &bloc_list);
	
	assert(bloc_list.magic == MAGIC_FL);
	
	tmp_bloc = bloc_list.next;
	bloc_list.next = bloc;
	read_bloc_n(current_vol, bloc, sizeof(bloc_list), (unsigned char*) &bloc_list);
	bloc_list.next = tmp_bloc;
	write_bloc_n(current_vol, bloc, sizeof(bloc_list), (unsigned char*) &bloc_list);
	
	super_bloc.nb_free_bloc++;
	save_super();}

    return 1;
}

void ls_super(unsigned int vol) {

    load_super(vol);

    printf("vol : %s\n", super_bloc.name);
    printf("serial : %d\n", super_bloc.serial);
    printf("num_bloc : %d\n", super_bloc.num_bloc);
    printf("nb_free_bloc : %d\n", super_bloc.nb_free_bloc);
    printf("first_free_bloc : %d\n", super_bloc.first_free_bloc);
}

int init_vol(int serial, char* name) {

    init_super(current_vol, serial, name);
	save_super();
    
    return 0;
}


