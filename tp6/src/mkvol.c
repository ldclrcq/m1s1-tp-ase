#include "mbr.h"
#include "super_b.h"
#include <stdlib.h>
#include <stdio.h>
#include "hardware.h"
#include "mkhd.h"

void main(int argc, char** argv) {
    init_master();
    load_mbr();
    unsigned int block_size = atoi(argv[1]);
    unsigned int cyl = atoi(argv[2]);
    unsigned int sec = atoi(argv[3]);
    unsigned int vol_number = gMBR.nbVol;
    char* name = "volume1"; 
    unsigned int serial = 141234;

    mkvol(block_size, cyl, sec, 1);
    init_vol(serial, "volume1");
    save_mbr();
}

