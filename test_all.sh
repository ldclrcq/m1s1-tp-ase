#!/usr/bin/env bash

########################################################################################################################
#				                          S C R I P T   S E T U P
########################################################################################################################

YELLOW='\033[1;33m'
GREEN='\033[0;32m'
TITLE_COLOR='\033[1;36m'
NC='\033[0m' # No Color

wait_for_keyboard() {
	echo -e "${GREEN}Press any key to run next test... ${NC}"
	read -p "" -n1 -s
	echo
}

echo -e "${TITLE_COLOR}${NC}"
echo -e "${TITLE_COLOR}${NC}"
echo -e "${TITLE_COLOR}      ___   _____ ______            __________${NC}"
echo -e "${TITLE_COLOR}     /   | / ___// ____/           /_  __/ __ \ ${NC}"
echo -e "${TITLE_COLOR}    / /| | \__ \/ __/    ______     / / / /_/ / ${NC}"
echo -e "${TITLE_COLOR}   / ___ |___/ / /___   /_____/    / / / ____/ ${NC}"
echo -e "${TITLE_COLOR}  /_/  |_/____/_____/             /_/ /_/${NC}"
echo -e "${TITLE_COLOR}${NC}"
echo -e "${TITLE_COLOR}${NC}"
echo -e "${TITLE_COLOR}${NC}"

########################################################################################################################
#				                          C O M P I L A T I O N
########################################################################################################################

echo -e "${YELLOW}Compiling everything :${NC}"

echo -e -n "${YELLOW} - TP1${NC}"
make -C tp1 clean install > /dev/null
echo -e "${GREEN}...OK${NC}"
echo -e -n "${YELLOW} - TP2${NC}"
make -C tp2 clean install > /dev/null
echo -e "${GREEN}...OK${NC}"
echo -e -n "${YELLOW} - TP3${NC}"
make -C tp3 > /dev/null
echo -e "${GREEN}...OK${NC}"
echo -e -n "${YELLOW} - TP4${NC}"
make -C tp4 install 1> /dev/null 2> /dev/null
echo -e "${GREEN}...OK${NC}"
echo -e -n "${YELLOW} - TP9${NC}"
make -C tp9 1> /dev/null 2> /dev/null
echo -e "${GREEN}...OK${NC}"
echo -e -n "${YELLOW} - TP10${NC}"
make -C tp10 1> /dev/null 2> /dev/null
echo -e "${GREEN}...OK${NC}"
echo

########################################################################################################################
#				                          T E S T S
########################################################################################################################

echo -e "${YELLOW}Testing all TPs...${NC}"
echo -e "${YELLOW}${NC}"

echo -e "${TITLE_COLOR}Testing TP1 - Gestion des contextes : retour vers un contexte${NC}"
echo
cd tp1
./tests.sh
cd ..

echo
echo -e "${TITLE_COLOR}Testing TP2 - Gestion de contextes : création d'un contexte d'exécution${NC}"
echo
./tp2/tp2
echo
wait_for_keyboard


echo
echo -e "${TITLE_COLOR}Testing TP3 - Gestion de contextes : ordonnancement, ordonnancement sur interruptions ${NC}"
echo
cd tp3
./tp3
cd ..
echo
wait_for_keyboard

echo
echo -e "${TITLE_COLOR}Testing TP4 - Gestion de contextes : synchronisation entre tâches ${NC}"
echo
cd tp4
./build/bin/prod_cons
cd ..
echo
wait_for_keyboard

echo
echo -e "${TITLE_COLOR}Testing TP9 - Protection de la mémoire. Isolation mémoire${NC}"
echo
cd tp9
./build/bin/mem
cd ..
echo
wait_for_keyboard

echo
echo -e "${TITLE_COLOR}Testing TP10 - Mémoire virtuelle et swap disque ${NC}"
echo
cd tp10
 ./mmu_manager | ./oracle
make clean > /dev/null
echo
echo -e "${YELLOW}Done !${NC}"
