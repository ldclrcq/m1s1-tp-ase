#include <stdlib.h>
#include <stdio.h>
#include "tp3.h"
#include "switch_to.h"
#include "hardware.h" 
#include "hwconfig.h" 

struct ctx_s ctx_ping;
struct ctx_s ctx_pong;

static void empty_it(void) {
    return;
}

static void timer_it() {
    static int tick = 0;
    fprintf(stderr, "hello from IT %d\n", ++tick);
    _out(TIMER_ALARM,0xFFFFFFFE);
}

int main() {
    create_ctx(16384, f_ping, NULL);
    create_ctx(16384, f_pong, NULL);

    unsigned int i;

    if (init_hardware(INIFILENAME) == 0) {
	fprintf(stderr, "Error in hardware initialization\n");
	exit(EXIT_FAILURE);
    }

    for (i=0; i<16; i++)
	IRQVECTOR[i] = empty_it;

    IRQVECTOR[TIMER_IRQ] = timer_it;
    _out(TIMER_PARAM,128+64+32+8);
    _out(TIMER_ALARM,0xFFFFFFFE);

    _mask(1);

    yield();

  exit(0);
}

void f_ping(void *arg) {
    printf("A\n");
    printf("B\n");
    printf("C\n");
}

void f_pong(void *arg) {
    printf("1\n");
    printf("2\n");
}
