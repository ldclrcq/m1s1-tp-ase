#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include "switch_to.h"

#define CTX_MAGIC 0xCAFEBABE

static struct ctx_s * current_ctx = NULL;
static struct ctx_s * ring = NULL;

void irq_disable() {
    _mask(15);
}

void irq_enable() {
    _mask(1);
}

int init_ctx(struct ctx_s *ctx, int stack_size, funct_t f, void *arg) {
  ctx->ctx_magic = CTX_MAGIC;
  ctx->ctx_base = malloc(stack_size);
  assert(ctx->ctx_base);
  ctx->ctx_esp = ctx->ctx_ebp = ctx->ctx_base + stack_size - 4;
  ctx->ctx_f = f;
  ctx->ctx_arg = arg;
  ctx->ctx_state = CTX_INIT;
}

int
create_ctx(int stack_size, funct_t f, void *args) {
    struct ctx_s * new;
    new = malloc(sizeof(struct ctx_s));
    assert(new);

    init_ctx(new, stack_size, f, args);

    if (!ring) {
        ring = new;
        new -> ctx_next = new;
    } else {
        new -> ctx_next = ring -> ctx_next;
        ring -> ctx_next = new;
    }
}

void yield() {
    if (current_ctx) {
        switch_to_ctx(current_ctx->ctx_next);
    } else if (ring){
        switch_to_ctx(ring);
    } else {
        exit(0);
    }
}

void switch_to_ctx(struct ctx_s *ctx) {

      while (ctx->ctx_state == CTX_END) {
          if (ctx->ctx_next == ctx) {
              //asm("movl %0, %%esp" "\n\t" "movl %1, %%ebp" : : "r" (main_esp), "r" (main_ebp) );
              return;
          }
          free(ctx->ctx_base);
          current_ctx->ctx_next = ctx->ctx_next;
          free(ctx);
          ctx = current_ctx->ctx_next;
      }

    assert(ctx->ctx_magic == CTX_MAGIC);
    assert(ctx->ctx_state == CTX_EXEC || ctx->ctx_state == CTX_INIT);

    if (current_ctx) {
      asm("movl %%esp, %0" "\n\t" "movl %%ebp, %1" : "=r" (current_ctx->ctx_esp), "=r" (current_ctx->ctx_ebp));
  }

    current_ctx = ctx;
    asm("movl %0, %%esp" "\n\t" "movl %1, %%ebp" : : "r" (ctx->ctx_esp), "r" (ctx->ctx_ebp) );

    if (current_ctx->ctx_state == CTX_INIT) {
      start_current_ctx();
  }



}

void start_current_ctx() {
  current_ctx->ctx_state = CTX_EXEC;
  current_ctx->ctx_f(current_ctx->ctx_arg);
  current_ctx->ctx_state = CTX_END;
  yield();
}
