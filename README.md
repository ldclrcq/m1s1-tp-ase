# TP ASE

Lucas Declercq & Paul Delafosse

## TPs

- [TP1 - Bibliothèque Try/Throw](tp1/README.md)
- [TP2 - Gestion de contextes : création d'un contexte d'exécution](tp2/README.md)
- [TP3 - Gestion de contextes : ordonnancement, ordonnancement sur interruptions](tp3/README.md)
- [TP4 - Gestion de contextes : synchronisation entre tâches](tp4/README.md)
- [TP5 - Première couche logicielle : accès au matériel ](tp5/README.md)
- [TP6 - Seconde couche logicielle : gestion de volumes ](tp6/README.md)
- [TP7 - Troisième couche logicielle, 1re partie : structure d'un volume (super bloc)](tp7/README.md)
- [TP8 - Troisième couche logicielle, 2e partie : structure d'un fichier (inode) ](tp8/README.md)
- [TP9 - Protection de la mémoire. Isolation mémoire  ](tp9/README.md)
- [TP10 - Mémoire virtuelle et swap disque](tp10/README.md)

## Test

Lancement d'une procédure de test sur tous les tps :

```
./test_all.sh
```