#include <stdio.h>
#include <stdlib.h>
#include "hardware.h"
#include "hwconfig.h"
#include "mi_kernel.h"
#include "mi_user.h"

struct tlb_entry_s {
	unsigned tlbe_rfu :8;
	unsigned tlbe_vpage :12;
	unsigned tlbe_ppage :8;
	unsigned tlbe_xwr :3;
	unsigned tlbe_access :1;
};

static int current_process;

int main(int argc, char **argv) {
	if(init_hardware("config/hwconfig.ini") == 0) {
		fprintf(stderr, "Error in hardware initialization\n");
		exit(EXIT_FAILURE);
	}

	IRQVECTOR[MMU_IRQ] = mmu_handler;
	IRQVECTOR[16] = switch_to_process0;
	IRQVECTOR[17] = switch_to_process1;
	_mask(0x1001);

	init();
}

static int ppage_of_vpage(int process, unsigned vpage) {
	if(vpage > N/2-1) {
		return -1;
	}

	if(process == 0) {
		return vpage + 1;
	} else if(process == 1) {
		return vpage + N/2 + 1;
	} else {
		return -1;
	}
}

static void mmu_handler(void) {
	unsigned int vaddr;
	unsigned int vpage;
	struct tlb_entry_s tlbe;

	vaddr = _in(MMU_FAULT_ADDR);

	if((vaddr & 0xFFFFFF) == ((int) virtual_memory)) {
		fprintf(stderr, "Not a valid Virtual Memory Address\n", vaddr);
		return;
	}

	vpage = vaddr >> 12 & 0xFFF;

	if(vpage>=N/2) {
		fprintf(stderr, "VPage is too big !", vpage, vaddr);
	}

	tlbe.tlbe_vpage = vpage;
	tlbe.tlbe_ppage = ppage_of_vpage(current_process, vpage);
	tlbe.tlbe_xwr = 7;
	tlbe.tlbe_access = 1;

	_out(TLB_ADD_ENTRY, *((int*)(&tlbe)));
}

static void switch_to_process0(void) {
	current_process = 0;
	_out(MMU_CMD, MMU_RESET);
}

static void switch_to_process1(void) {
	current_process = 1;
	_out(MMU_CMD, MMU_RESET);
}