#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hwconfig.h"
#include "hardware.h"
#include "mi_user.h"

int sum(void *ptr) {
	int i;
	int sum = 0;

	for (i = 0; i < PAGE_SIZE * N / 2; i++) {
		sum += ((char*)ptr)[i];
	}

	return sum;
}

void init(void) {
	void *ptr;
	int res;

	ptr = virtual_memory;

	printf("Page size : %d\n", PAGE_SIZE);

	_int(16);
	memset(ptr, 1, PAGE_SIZE * N/2);

	_int(17);
	memset(ptr, 3, PAGE_SIZE * N/2);

	_int(16);
	res = sum(ptr);
	printf("Process 0's result : %d\n",res);

	_int(17);
	res = sum(ptr);
	printf("Process 1's result : %d\n",res);
}