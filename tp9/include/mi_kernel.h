//
// Created by lucas on 12/11/18.
//

#ifndef TP_ASE_MI_KERNEL_H
#define TP_ASE_MI_KERNEL_H


static int ppage_of_vpage(int process, unsigned vpage);
static void mmu_handler(void);
static void switch_to_process0(void);
static void switch_to_process1(void);

#endif //TP_ASE_MI_KERNEL_H
