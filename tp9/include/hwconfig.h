/* ------------------------------
   $Id: hw_config.h 86 2007-06-01 14:34:35Z skaczmarek $
   ------------------------------------------------------------

   Fichier de configuration des acces au materiel

   Philippe Marquet, march 2007

   Code au niveau applicatif la description du materiel qui est fournie
   par hardware.ini
   
*/

#ifndef _HW_CONFIG_H_
#define _HW_CONFIG_H_

#define HWCONFIG "config/hwconfig.ini"

/* Horloge */
#define TIMER_CLOCK	0xF0
#define TIMER_PARAM     0xF4
#define TIMER_ALARM     0xF8
#define TIMER_IRQ	2
#define HDA_MAXCYLINDER 16
#define HDA_MAXSECTOR   16
#define HDA_SECTORSIZE  256

#define CMD_REG 0x3F6
#define HDA_CMDREG 0x3F6
#define HDA_DATAREGS 0x110
#define HDA_IRQ 14
#define PAGE_SIZE 127

#define N 200

#endif
