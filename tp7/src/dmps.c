#include <stdio.h> #include "hardware.h" #include "hwconfig.h"
#include "drive.h"
#include "dump.h"

int main() {
    
    unsigned char buffer[256];
    setup(); 
    read_sector(0, 1, buffer);
    dump(buffer, HDA_SECTORSIZE, 1, 1);
}
