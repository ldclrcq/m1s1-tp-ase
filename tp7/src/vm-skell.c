#include <stdio.h> 
#include <stdlib.h> 
#include <string.h>
#include "mbr.h"
#include "drive.h"
#include "mkhd.h"
#include "super_b.h"
#include "vm-skell.h"

struct _cmd {
    char *name;
    void (*fun) (struct _cmd *c);     
    char *comment; 
}; 
static void list(struct _cmd *c); 
static void new(struct _cmd *c); 
static void del(struct _cmd *c); 
static void info(struct _cmd *c); 
static void help(struct _cmd *c) ; 
static void save(struct _cmd *c); 
static void quit(struct _cmd *c); 
static void xit(struct _cmd *c); 
static void none(struct _cmd *c) ; 
static struct _cmd commands [] = {     
    {"list", list, 	"display the partition table"},     
    {"info", info, 	"display information on a specific partition"},     
    {"new",  new,	"create a new partition"},     
    {"del",  del,	"delete a partition"},     
    {"save", save,	"save the MBR"},     
    {"quit", quit,	"save the MBR and quit"},     
    {"exit", xit,	"exit (without saving)"},     
    {"help", help,	"display this help"},     
    {0, none, 		"unknown command, try help"} 
} ; 

static void execute(const char *name) {
    struct _cmd *c = commands;       
    while (c->name && strcmp (name, c->name))
        c++;     (*c->fun)(c); 
} 

static void loop(void) {     
    char name[64];
    while (printf("> "), 
            scanf("%62s", name) == 1)
        execute(name) ;
} 

static void list(struct _cmd *c) {     
    ls_vol();
} 

static void new(struct _cmd *c) {     
    printf("%s Create a new partition\n", c->name); 
    unsigned int block_size = get_arg("Enter block size : \n");
    unsigned int cyl = get_arg("Enter target cylinder : \n");
    unsigned int sec = get_arg("Sector number : \n");
    unsigned int vol_number = gMBR.nbVol;
//    char* name = getchar_arg("Enter volume name : \n");
    unsigned int serial = get_arg("Enter volume serial number : \n");

    mkvol(block_size, cyl, sec, 1);
    init_vol(serial, "poet");
} 

static void del(struct _cmd *c) {     
    
    unsigned int vol_n = get_arg("Enter the number of the volume to delete : \n");
    printf("%s deleting volume %d \n", c->name, vol_n); 
    del_vol(vol_n);
} 

static void save(struct _cmd *c) {     
    printf("%s Saving the mbr\n", c->name); 
    save_mbr();
} 

static void info(struct _cmd *c) {
    unsigned int vol_n = get_arg("Enter the number of the volume : \n");
    if (vol_n >= gMBR.nbVol || vol_n < 0) {
        printf("No such volume");
        return;
    }
    ls_super(vol_n); 
}

static void quit(struct _cmd *c) {     
    save_mbr();
    exit(EXIT_SUCCESS); 
} 

static void do_xit() {     
    exit(EXIT_SUCCESS); 
} 

static void xit(struct _cmd *dummy) {     
    do_xit(); 
} 

static void help(struct _cmd *dummy) {     
    struct _cmd *c = commands;       
    for (; c->name; c++) 	
        printf ("%s\t-- %s\n", c->name, c->comment); 
}

static void
none(struct _cmd *c)
{
    printf ("%s\n", c->comment) ;
}

unsigned int get_arg(char* msg) {
    unsigned int arg = -1;
    while(arg == -1) {
        printf("%s", msg); 
        scanf("%d", &arg);
    }
    return arg; 
}

char* getchar_arg(char *msg) {
    char* arg = malloc(30);
    int flag = 0;
        printf("%s", msg); 
        scanf("%s", &arg);
    return arg; 
}

int
main(int argc, char **argv)
{
    init_master();
    load_mbr();
    loop();

    do_xit();

    exit(EXIT_SUCCESS);
}



