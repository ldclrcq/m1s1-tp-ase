#ifndef TP4_DUMP_H
#define TP4_DUMP_H

void dump(unsigned char *buffer, unsigned int buffer_size, int ascii_dump, int octal_dump);

#endif //TP4_DUMP_H
