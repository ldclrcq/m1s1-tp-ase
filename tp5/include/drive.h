#ifndef TP4_DRIVE_H
#define TP4_DRIVE_H

void goto_sector(int cyl, int sect);
void read_sector(unsigned int cylinder, unsigned int sector, unsigned char *buffer);
void read_sector_size(unsigned int cylinder, unsigned int sector, int size, unsigned char *buffer);
void write_sector(unsigned int cylinder, unsigned int sector, unsigned char* buffer);
void write_sector_size(unsigned int cylinder, unsigned int sector, int size, unsigned char* buffer);
void format_sector(unsigned int cylinder, unsigned int sector, int value);

#endif //TP4_DRIVE_H
