#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "drive.h"
#include "hardware.h"
#include "hwconfig.h"


void empty_it() {
return;
}

void setup() {

    int i;

    if(init_hardware(HWCONFIG) == 0) {
        fprintf(stderr, "Error in hardware initialization\n");
        exit(1);
    }

	for(i=0; i<16; i++)
		IRQVECTOR[i] = empty_it;

	_mask(1);
}



void goto_sector(int cyl, int sect) {
    _out(HDA_DATAREGS, (cyl>>8 & 0xFF));
    _out(HDA_DATAREGS + 1, cyl & 0xFF);
    _out(HDA_DATAREGS + 2, (sect>>8 & 0xFF));
    _out(HDA_DATAREGS + 3, sect & 0xFF);

    _out(HDA_CMDREG, CMD_SEEK);
    _sleep(HDA_IRQ);
}

void read_sector(unsigned int cylinder, unsigned int sector, unsigned char *buffer) {
    read_sector_size(cylinder, sector, HDA_SECTORSIZE, buffer);
}

void read_sector_size(unsigned int cylinder, unsigned int sector, int size,  unsigned char *buffer) {
    assert(cylinder < HDA_MAXCYLINDER);
    assert(sector < HDA_MAXSECTOR);
    assert(size <= HDA_SECTORSIZE);

    goto_sector(cylinder, sector);

    _out(HDA_DATAREGS, 0);
    _out(HDA_DATAREGS + 1, 1);
    _out(HDA_CMDREG, CMD_READ);
    _sleep(HDA_IRQ);

    memcpy(buffer, MASTERBUFFER, size);

}

void write_sector(unsigned int cylinder, unsigned int sector, unsigned char* buffer) { 
    goto_sector(cylinder, sector); 

    _out(HDA_DATAREGS, 0); 
    _out(HDA_DATAREGS+1, 1);

    for(int i = 0; i< HDA_SECTORSIZE; i++) {
        MASTERBUFFER[i] = buffer[i];
    }

    _out(HDA_CMDREG, CMD_WRITE);
    _sleep(HDA_IRQ); 

}

void write_sector_size(unsigned int cylinder, unsigned int sector, int size, unsigned char* buffer) {
    
    _out(HDA_DATAREGS, 0); 
    _out(HDA_DATAREGS+1, 1);

    for(int i = 0; i< size; i++) {
        MASTERBUFFER[i] = buffer[i];
    }
    
    _out(HDA_CMDREG, CMD_WRITE);
    _sleep(HDA_IRQ); 

}

void format_sector(unsigned int cylinder, unsigned int sector, int value) {
    goto_sector(cylinder, sector); 

    _out(HDA_DATAREGS, 0);
    _out(HDA_DATAREGS+1, 1); 
    _out(HDA_DATAREGS+2, (value & 0xFF00) >> 8);
    _out(HDA_DATAREGS+3, (value & 0xFF));
    _out(HDA_DATAREGS+4, (value & 0xFF00) >> 8);
    _out(HDA_DATAREGS+5, (value & 0xFF));

    _out(HDA_CMDREG, CMD_FORMAT);
    _sleep(HDA_IRQ); 
}
