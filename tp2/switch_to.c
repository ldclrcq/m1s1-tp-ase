#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include "switch_to.h"

#define CTX_MAGIC 0xCAFEBABE
#define MAX_ITERATION_COUNT 10

static struct ctx_s *current_ctx = NULL;

static int counter = 0;

int init_ctx(struct ctx_s *ctx, int stack_size, funct_t f, void *arg) {
	ctx->ctx_magic = CTX_MAGIC;
	ctx->ctx_base = malloc(stack_size);
	assert(ctx->ctx_base);
	ctx->ctx_esp = ctx->ctx_ebp = ctx->ctx_base + stack_size - 4;
	ctx->ctx_f = f;
	ctx->ctx_arg = arg;
	ctx->ctx_state = CTX_INIT;
}

void switch_to_ctx(struct ctx_s *ctx) {
	if (counter <= MAX_ITERATION_COUNT) {
		counter++;
		assert(ctx->ctx_magic == CTX_MAGIC);
		assert(ctx->ctx_state == CTX_EXEC || ctx->ctx_state == CTX_INIT);

		if (current_ctx) {
			asm("movl %%esp, %0" "\n\t" "movl %%ebp, %1" : "=r" (current_ctx->ctx_esp), "=r" (current_ctx->ctx_ebp));
		}

		current_ctx = ctx;
		asm("movl %0, %%esp" "\n\t" "movl %1, %%ebp" : : "r" (ctx->ctx_esp), "r" (ctx->ctx_ebp));

		if (current_ctx->ctx_state == CTX_INIT) {
			start_current_ctx();
		}
	} else {
		exit(0);
	}
}

void start_current_ctx() {
	current_ctx->ctx_state = CTX_EXEC;
	current_ctx->ctx_f(current_ctx->ctx_arg);
	current_ctx->ctx_state = CTX_END;

	printf("ctx is over\n");
}
